//
//  ViewController.swift
//  Tasks
//
//  Created by A on 30/01/2016.
//  Copyright © 2016 A. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UITableViewController {
    
    @IBOutlet var indicator:UIActivityIndicatorView!
    @IBOutlet var segmentedControl:UISegmentedControl!
    
    var tasks = [NSManagedObject]()
    let url = "https://bitbucket.org/fatunicorn/iostest/raw/78f902ddb31ac96bb23b901054442dee52664271/data/tasklist.json"
    
    var cache = [String: UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Show the loading icon
        indicator.startAnimating()
        
        getTasks()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Set up our table view cell
        let cell:TableCell = self.tableView.dequeueReusableCellWithIdentifier("TableCell", forIndexPath: indexPath) as! TableCell
        
        // Load task from core data and display task name in the tableviewcell label
        let task = tasks[indexPath.row]
        cell.label.text = task.valueForKey("name") as? String
        
        // Load image from cache
        cell.picture.image = self.cache[task.valueForKey("name") as! String]
        
        // Fill up as much of the imageview as we can
        cell.picture.contentMode = .ScaleToFill
        
        
        // Display a tick next to tasks that have been marked as done
        if (task.valueForKey("done") as? Bool == true) {
            //cell.accessoryType = .Checkmark
        }
        
        return cell
        
}
    
    func getTasks() -> Void {
        
        // Define for use later on
        let location = NSURL.init(string: url)
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext

        let session = NSURLSession.sharedSession()
        session.dataTaskWithURL(location!) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            if(error == nil){
                let status = (response as! NSHTTPURLResponse).statusCode
                if(status == 200) {
                    if(data != nil) {
                        do { // Try to parse the JSON and catch if there's an error
                            let json = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as! NSArray
                            
                            // Get our entity described in the XCode project
                            let entity =  NSEntityDescription.entityForName("Task",inManagedObjectContext:managedContext)
                            
                            // Loop through JSON results and add to core data
                            for item in json {
                                do {
                                    // Check if this task has already been added into core data
                                    // to avoid repeatadly adding the same tasks over and over
                                    let request = NSFetchRequest.init(entityName: "Task")
                                    request.predicate = NSPredicate(format: "name = %@", item["task"] as! String)
                                    let results = try managedContext.executeFetchRequest(request)
                                    
                                    // Declare these to make things simpler later on
                                    let name = item["task"] as! String
                                    let image = item["image"] as! String
                                    
                                    // If the image hasn't been previously loaded
                                    if(self.cache[name] == nil) {
                                        
                                        // If no image specified, load default image
                                        if(image == "") {
                                            self.cache[name] = UIImage.init(named: "default.gif")
                                        }
                                            
                                        // Otherwise attempt to load the image from the internet
                                        else {
                                            let data = NSData.init(contentsOfURL: NSURL.init(string: image)!)
                                            if(data != nil) {
                                                let loadedImage = UIImage.init(data: data!)
                                                self.cache[name] = loadedImage; // Store in cache to use next time
                                            }
                                        }
                                    }
                                    
                                    if(results.count == 0) {
                                        // No matching results found, so lets insert to core data
                                        let task = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
                                        task.setValue(item["task"], forKey: "name")
                                        task.setValue(item["image"], forKey: "image")
                                        task.setValue(item["done"], forKey: "done")
                                        
                                        
                                        try managedContext.save() // Save the context
                                        self.tasks.append(task) // Push to our array that stores the tasks
                                    }
                                }
                                catch {
                                    self.showError("Error saving to database")
                                }
                                
                            }
                            
                        }
                        // Self explanatory
                        catch {
                            self.showError("Problem reading data")
                        }
                    }
                }
            }
            
            else {
                // Display error message
                 self.showError("There was a problem obtaining data from the internet. Please check your connection")
            }
            
           let fetch = NSFetchRequest(entityName: "Task")
            
            do {
                // Grab all the tasks from core data
                let results = try managedContext.executeFetchRequest(fetch)
                self.tasks = results as! [NSManagedObject] // Update our copy of the data
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    self.tableView.reloadData() // Refresh the table to display the updated data
                    self.indicator.stopAnimating()
                    self.indicator.hidden = true
                }
            }
            catch  {
                self.showError("Something went wrong...")
            }
            
        }.resume()
        
    }
    
    func showError(error: String) -> Void {
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            
            // Let the user know there was a problem
            let alert = UIAlertController(title: "Error", message:error, preferredStyle: UIAlertControllerStyle.Alert)
            let ok = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
            
            // Add action to dismiss the alert
            alert.addAction(ok)
            
            // Present the alert
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func segmentValueChanged() -> Void {
        
        self.indicator.hidden = false
        self.indicator.startAnimating()
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            
            // Get the app delegate and managed context
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let managedContext = appDelegate.managedObjectContext
            
            // Task will be the table we fetch data from
            let fetch = NSFetchRequest(entityName: "Task")
            
            // If to do segment is selected, search for tasks not yet done
            if(self.segmentedControl.selectedSegmentIndex == 0) {
                fetch.predicate = NSPredicate(format: "done == false", argumentArray: [])
            }
            
            // If done is selected, we search for completed tasks
            if (self.segmentedControl.selectedSegmentIndex == 1) {
                fetch.predicate = NSPredicate(format: "done == true", argumentArray: [])
            }
            
            // Predicate is only added when to-do/done is selected
            // So here sometimes we will obtain ALL the results if no segment selected
            do {
                // Execute the results and update the tasks array
                let results = try managedContext.executeFetchRequest(fetch)
                self.tasks = results as! [NSManagedObject]
            }
            
            catch {
                self.showError("Something went wrong...")
            }
            dispatch_async(dispatch_get_main_queue()) {
                
                // Get back to the main queue and update some UI
                self.indicator.stopAnimating()
                self.indicator.hidden = true
                self.tableView.reloadData()
            }
        }
        
    }


}


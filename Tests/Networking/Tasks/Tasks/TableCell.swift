//
//  TableCell.swift
//  Tasks
//
//  Created by A on 30/01/2016.
//  Copyright © 2016 A. All rights reserved.
//

import UIKit

class TableCell: UITableViewCell {
    
    @IBOutlet var picture: UIImageView!
    @IBOutlet var label: UILabel!

}

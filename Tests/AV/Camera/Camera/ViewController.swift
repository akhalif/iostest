//
//  ViewController.swift
//  Camera
//
//  Created by A on 30/01/2016.
//  Copyright © 2016 A. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    // Declare some AV Foundation variables for later use
    var session: AVCaptureSession?
    var image: AVCaptureStillImageOutput?
    var preview: AVCaptureVideoPreviewLayer?
    
    @IBOutlet var snap: UIButton?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create a new capture session
        let session = AVCaptureSession()
        session.sessionPreset = AVCaptureSessionPresetPhoto
        
        
        // Find the front facing camera, no need for loops, a bit hard to read though
        let frontCamera = AVCaptureDevice.devices().filter{ $0.hasMediaType(AVMediaTypeVideo) && $0.position == AVCaptureDevicePosition.Front}.first as! AVCaptureDevice;
        
        do {
            // Set the front facing camera as the input
            let input = try AVCaptureDeviceInput(device: frontCamera)
            session.addInput(input)
            
            // Get the image output since we're aiming to display a live preview
            image = AVCaptureStillImageOutput()
            image!.outputSettings[AVVideoCodecKey] = AVVideoCodecJPEG
            session.addOutput(image)
            
            // Set up a preview layer, populate this view's layer with the preview layer
            preview = AVCaptureVideoPreviewLayer(session: session)
            preview?.frame = self.view.layer.frame
            self.view.layer.addSublayer(preview!)
            
            // Time to start receiving data from the camera!
            session.startRunning()
            
            // Make sure the button never goes behind the preview
            self.view.bringSubviewToFront(snap!)
            
        }
            
        catch {
            print("Error connecting to the front facing camera")
        }
    }
    
    @IBAction func buttonPressed() {
        if let connection =  image?.connectionWithMediaType(AVMediaTypeVideo) {
            image?.captureStillImageAsynchronouslyFromConnection(connection, completionHandler: { (buffer, error) -> Void in
                
                // Load JPEG data
                let imgContents = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(buffer)
                
                // Initialize a provider
                let prov = CGDataProviderCreateWithCFData(imgContents)
                
                // Time to create our JPEG image
                let cgImg = CGImageCreateWithJPEGDataProvider(prov, nil, true, .RenderingIntentDefault)
                
                // Convert the core graphics JPEG into a UIIMage to save
                let photo = UIImage(CGImage: cgImg!)
                
                // Save the image to the user's photo album
                UIImageWriteToSavedPhotosAlbum(photo, nil, nil, nil)
                
                // Let the user know their selfie was saved
                let alert = UIAlertController(title: "Saved!", message:"Your selfie has been saved!", preferredStyle: UIAlertControllerStyle.Alert)
                let ok = UIAlertAction(title: "Yay", style: UIAlertActionStyle.Default, handler: nil)
                
                // Add action to dismiss the alert
                alert.addAction(ok)
                
                // Present the alert
                self.presentViewController(alert, animated: true, completion: nil)
                
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

